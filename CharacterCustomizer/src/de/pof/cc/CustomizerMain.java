package de.pof.cc;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class CustomizerMain extends JFrame implements Runnable{
	private static final long serialVersionUID = 1L;

	public static BufferedImage saveChar;
	public static BufferedImage Char;	
	public static JTextField hairCustomizer = new JTextField("Insert Color Key for the hair");
	public static JTextField skinCustomizer = new JTextField("Insert Color Key for the skin");
	public static JTextField eyeCustomizer = new JTextField("Insert Color Key for the eyes");
	public static JTextField jacketCustomizer = new JTextField("Insert Color Key for the jacket");
	public static JTextField shortsCustomizer = new JTextField("Insert Color Key for the shorts");
	public static JTextField beltCustomizer = new JTextField("Insert Color Key for the belt");
	
	public static void main(String[] args) {
		final CustomizerMain cm = new CustomizerMain();
		loadChar();
		new Thread(new Runnable(){
			@Override
			public void run() {
				drawImage(cm);
			}
		}).start();
	}
	
	public CustomizerMain(){
		super("PlayerCustomizer by PhoenixofForce");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(null);
		setVisible(true);
		setResizable(false);
		final Insets i = getInsets();
		setSize(425 + i.right + i.left, 500 + i.top + i.bottom);
		
		add(hairCustomizer);
		hairCustomizer.setBounds(250, 500 / 6 * 0, 175, 500 / 6);
		addListener(hairCustomizer, new Color(0x020202));
		
		add(skinCustomizer);
		skinCustomizer.setBounds(250, 500 / 6 * 1, 175, 500 / 6);
		addListener(skinCustomizer, new Color(0x5e5e5e));
		
		add(eyeCustomizer);
		eyeCustomizer.setBounds(250, 500 / 6 * 2, 175, 500 / 6);
		addListener(eyeCustomizer, new Color(0x797979));
		
		add(jacketCustomizer);
		jacketCustomizer.setBounds(250, 500 / 6 * 3, 175, 500 / 6);
		addListener(jacketCustomizer, new Color(0x999999));
		
		add(shortsCustomizer);
		shortsCustomizer.setBounds(250, 500 / 6 * 4, 175, 500 / 6);
		addListener(shortsCustomizer, new Color(0xc1c1c1));
		
		add(beltCustomizer);
		beltCustomizer.setBounds(250, 500 / 6 * 5, 175, 500 / 6);
		addListener(beltCustomizer, new Color(0xdadada));
	}
	
	public static void loadChar(){
		try {
			saveChar = ImageIO.read(ClassLoader.getSystemResource("de/pof/cc/res/Character.png"));
			Char = ImageIO.read(ClassLoader.getSystemResource("de/pof/cc/res/Character.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void drawImage(CustomizerMain cm){
		Graphics g = cm.getGraphics();
		while(true){
			
			g.drawImage(Char, cm.getInsets().left + 50, cm.getInsets().top + 150, null);
			
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void changeColor(Color a, Color b, BufferedImage image, BufferedImage saveImage){
		for(int x = 0; x < image.getWidth(); x++){
			for(int y = 0; y < image.getHeight(); y++){
				if(new Color(saveImage.getRGB(x, y)).equals(a)){
					image.setRGB(x, y, b.getRGB());
				}
			}
		}
	}
	
	public static void addListener(final JTextField a, final Color b){
		a.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				int color;
				try{
					color = Integer.valueOf(a.getText(), 16);
					
					Color c = new Color(color);
					
					changeColor(b, c, Char, saveChar);
				}catch(Exception e1){
					e1.printStackTrace();
				}
				
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				int color;
				try{
					color = Integer.valueOf(a.getText(), 16);
					
					Color c = new Color(color);
					
					changeColor(b, c, Char, saveChar);
				}catch(Exception e1){
					e1.printStackTrace();
				}
				
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				int color;
				try{
					color = Integer.valueOf(a.getText(), 16);
					
					Color c = new Color(color);
					
					changeColor(b, c, Char, saveChar);
				}catch(Exception e1){
					e1.printStackTrace();
				}
				
			}
		});
	}
	
	@Override
	public void run() {		
	}
}